package com.example.jkuswanto.proj_2;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.wearable.view.WatchViewStub;
import android.widget.TextView;

public class NotificationActivity extends Activity {

    private TextView mTextView;

    private int notification_id = 1;
    private final String NOTIFICATION_ID = "notification_id";

    private NotificationCompat.Builder not_builder;
    private NotificationManagerCompat not_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
            }
        });

        // Make intent, not sure what this is for
        Intent open_intent = new Intent(this, ActivatedActivity.class);
        open_intent.putExtra(NOTIFICATION_ID, notification_id);
        PendingIntent pending_intent = PendingIntent.getActivity(this, 0, open_intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Create notification, add attributes
        not_builder = new NotificationCompat.Builder(this)
            //.setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle(getString(R.string.quake))
            .setContentText(getString(R.string.temp))
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(true)
            .setContentIntent(pending_intent);

        // Instantiate notification manager to start/stop
        not_manager = NotificationManagerCompat.from(this);
    }
}
