package com.example.jkuswanto.proj_2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.TextView;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends Activity implements SensorEventListener {

    // Sensor stuff
    private SensorManager mSensorManager;
    private Sensor acceleration;

    private float cur_x;
    private float cur_y;
    private float cur_z;

    private String TAG;
    private String CAPABILITY_NAME = "get_quake";

    //Message stuff
    private GoogleApiClient mGoogleApiClient;
    private CapabilityApi.GetCapabilityResult node;
    private String RECEIVER_SERVICE_PATH;

    @Override
    public void onAccuracyChanged (Sensor sensor, int accuracy){

    }

    @Override
    public void onSensorChanged(SensorEvent event){
        if (event.values[1] != cur_y && event.values[2] != cur_z){
            // The watch is shaking, send a message to the phone

            // Step 4: Sending the Message
            // Send phone a notification when watch is shaken
            // Wearable.MessageApi.sendMessage(mGoogleApiClient, nodeId, RECEIVER_SERVICE_PATH, new byte[3]);

            Wearable.MessageApi.sendMessage(mGoogleApiClient, node.toString(), RECEIVER_SERVICE_PATH, new byte[3]);

            Log.d(TAG, "x:" + cur_x + ";y:" + cur_y + ";z:" + cur_z);
        }
        cur_x = event.values[0];
        cur_y = event.values[1];
        cur_z = event.values[2];
    }

    @Override
    public void onPause(){
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        mSensorManager.registerListener(this, acceleration, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_activity_main);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        acceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Create the Wearable Data Layer

        // Creating a Google API
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle connectionHint){
                    Log.d(TAG, "onConnected: " + connectionHint);

                    // What do we need to do when the phone and watch are connected?
                    // Start listening for stuff on the watch?
                }
                @Override
                public void onConnectionSuspended(int cause){
                    Log.d(TAG, "onConnectionSuspended: " + cause);
                }
            })
            .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult result) {
                    Log.d(TAG, "onConnectionFailed: " + result);
                }
            })
            .addApi(Wearable.API)
            .build();

        // Step 3: Discovering the Handheld
        node = Wearable.CapabilityApi.getCapability(mGoogleApiClient, CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE)
                .await();

    }

}
