package com.example.jkuswanto.proj_2;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

/**
 * Created by jkuswanto on 10/13/2015.
 */
public class WatchListenerService extends WearableListenerService {

    private static final String START_ACTIVITY = "/start_activity";

    @Override
    public void onMessageReceived(MessageEvent m_event){
        if (m_event.getPath().equalsIgnoreCase(START_ACTIVITY)){
            String value = new String(m_event.getData(), StandardCharsets.UTF_8);

            // Intent for notification
            Intent intent_not = new Intent(this, NotificationActivity.class);
            intent_not.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent_not);

            //Intent for sensing
            Intent intent_sense = new Intent(this, MainActivity.class);
            intent_sense.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent_sense);

        } else {
            super.onMessageReceived(m_event);
        }
    }
}
