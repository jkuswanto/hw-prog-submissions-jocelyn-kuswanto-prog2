package com.example.jkuswanto.proj_2;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;

public class MyService extends WearableListenerService {

    private String TAG;

    // Step 5: Receive a Message
    // This class listens for important data later events

    private static final String RECEIVER_SERVICE_PATH = "/receiver-service";
    private GoogleApiClient mApiClient;

    private ServiceConnection mServiceConnection;

    // GET request stuff
    private String url = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=%s&endtime=%s";
    private String start_time = String.format("%tFT%<tRZ",
            Calendar.getInstance(TimeZone.getTimeZone("Z")));;
    private String end_time = "";
    private HttpURLConnection connection = null;
    private static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();

    @Override
    public void onCreate() {

        super.onCreate();

        // Set up GoogleAPI
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint){
                    }

                    @Override
                    public void onConnectionSuspended(int cause){
                    }
                })
                .build();

        // Set up a service connection
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //mServiceConnection = ((MyService.MyBinder)service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mServiceConnection = null;
            }
        };

    }

    @Override
    public void onMessageReceived(MessageEvent m_event){
        Log.d(TAG, "Got a message");

        // What do we want to happen when we receieve a message from the watch?
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
       //Need to do something useful
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.i("tag", "Work");
                    if (end_time != "") {
                        start_time = end_time;
                    }
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    end_time = String.format("%tFT%<tRZ",
                            Calendar.getInstance(TimeZone.getTimeZone("Z")));

                    url = String.format(url, start_time, end_time);
                    url.replaceAll("\\+", "%2B");
                    try {
                        Log.i("tag", url);
                        URL query = new URL(url);

                        connection = (HttpURLConnection) query.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                        connection.setRequestProperty("Content-Language", "en-US");

                        connection.setUseCaches(false);
                        connection.setDoInput(true);

                        // Sends the request
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.close();

                        // Get response
                        InputStream is = connection.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                        StringBuilder response = new StringBuilder();
                        String line;
                        while((line=rd.readLine()) != null) {
                            response.append(line);
                        }
                        String stream = response.toString();
                        JSONObject jObj = new JSONObject(stream);
                        if (jObj.getJSONArray("features").length() != 0 ){
                            // We have an earthquake! Send a notification to the watch

                        }
                        rd.close();
                    } catch (Exception e){
                        e.printStackTrace();
                    } finally {
                        if (connection != null){
                            connection.disconnect();
                        }
                    }
                }
            }
        }).start();

        return START_STICKY;
    }

    // Use this to send message to WatchListener
    private void sendMessage (final String path, final String text){

    }
}
